function initScripts() {
menuPanel();
renderObjXaxis();
renderObjYaxis();
renderObjPerspective();
renderObjfoundation();

renderObjhouse();
renderObjLfloor();
renderObjtransmiter();
renderObjtable();
renderObjMdoor();
renderObjcityWest();
renderObjradio();
renderObjchair();
renderObjmic();

renderObjOwallsRx();
renderObjOwallsRy();
renderObjoutL1Wall();
renderObjoutL2Wall();
renderObjSfloor();
renderObjFfloor();
renderObjStairsWall();
renderObjoutL2Wall();
renderObjstairs();
renderObjstairs2();


shell();
}
$(document).ready(function(){
initScripts();
    });
//Autor: Manuel Janz Pereira necasjanz@gmail.com
