var FogX = '0';
var FogY = '0';
var fogX = '20%';
var fog1X = '20%';
var fog2X = '20%';
var fog3X = '20%';
var fog4X = '20%';
var fog5X = '20%';
var fog6X = '20%';
var fog7X = '20%';
var fog8X = '20%';
var fog9X = '20%';
/////////////////////////

var fogY = '20%';
var fogPer = '2000';
var fogRy = '0';
var fogRx = '60';
var fogSkewX = '0';
var fogSkewY = '0';
var fogZ = '0';
var fogtX = '0';
function renderObjFog() {
var $fog = $("#rotation");
$fog.append("<div id='fog'><div id='fog1'></div><div id='fog2'></div><div id='fog3'></div><div id='fog4'></div><div id='fog5'></div><div id='fog6'></div><div id='fog7'></div><div id='fog8'></div><div id='fog9'></div></div>")
};
var interval = setInterval(function() {
$("#fog").css({  'z-index':'10','opacity':'1','z-index':'1','position': 'absolute','overflow': 'hidden','left': FogX ,'top': FogY});
$("#fog1").css({  'z-index':'100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 42px','height': '6%', 'width': '6%','left': '50%','top':'72%','animation-name': 'Fog', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog2").css({  'z-index':'-12','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '4%', 'width': '6%','left': fog2X,'top':'71%','animation-name': 'Fog1', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog3").css({  'z-index':'-100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '6%', 'width': '23%','left': fog3X,'top':'75%','animation-name': 'Fog2', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog4").css({  'z-index':'-100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '6%', 'width': '23%','left': fog4X,'top':'71%','animation-name': 'Fog3', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog5").css({  'z-index':'-100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '6%', 'width': '23%','left': fog5X,'top':'72%','animation-name': 'Fog4', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog6").css({  'z-index':'-100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '6%', 'width': '23%','left': fog6X,'top':'71%','animation-name': 'Fog5', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog7").css({  'z-index':'-100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '6%', 'width': '23%','left': fog7X,'top':'73%','animation-name': 'Fog6', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog8").css({  'z-index':'-100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '6%', 'width': '23%','left': fog8X,'top':'72%','animation-name': 'Fog7', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
$("#fog9").css({  'z-index':'-100','opacity':'0.9','z-index':'1','position': 'absolute','overflow': 'hidden','background': 'radial-gradient(ellipse at center, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 1%,rgba(255,255,255,1) 3%,rgba(255,255,255,0)61%)','border-radius': '100px / 50px','height': '6%', 'width': '23%','left': fog9X,'top':'70%','animation-name': 'Fog8', 'animation-duration': '6s', 'animation-iteration-count': '3', 'animation-direction': 'alternate','opacity': '1','background-size': '100% 100%', '-webkit-transform': 'perspective(' + fogPer + 'px) rotateY(' + fogRy + 'deg) rotateX(' + fogRx + 'deg) translateZ(' + fogZ + 'px) skewX(' + fogSkewX + 'deg) skewY(' + fogSkewY + 'deg) translateX(' + fogtX + ')'});
document.getElementById("fog1").style.animationIterationCount = "infinite";
document.getElementById("fog2").style.animationIterationCount = "infinite";
document.getElementById("fog3").style.animationIterationCount = "infinite";
document.getElementById("fog4").style.animationIterationCount = "infinite";
document.getElementById("fog5").style.animationIterationCount = "infinite";
document.getElementById("fog6").style.animationIterationCount = "infinite";
document.getElementById("fog7").style.animationIterationCount = "infinite";
document.getElementById("fog8").style.animationIterationCount = "infinite";
document.getElementById("fog9").style.animationIterationCount = "infinite";
FogX =-rotationX + 210;
FogY =-rotationY + 170;
}, refreshStairs);
