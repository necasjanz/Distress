function menuPanel() {
var $panel = $("#frame");
$panel.append("<ul id='panel'></ul>");

var $panelcont = $("#panel");
$panelcont.append("<li class='dropdown'><a href='javascript:void(0)' class='dropbtn'>File</a><div class='dropdown-content'><a href='#'>New File</a><a href='#'>Open File</a><a href='#'>Save</a><a href='#'>Save As...</a></div></li>");
$panelcont.append("<li class='dropdown'><a href='javascript:void(0)' class='dropbtn'>Edit</a><div class='dropdown-content'><a href='#'>Undo</a><a href='#'>Redo</a><a href='#' id='Calibar'>Calibrate</a></div></li>");
$panelcont.append("<li class='dropdown'><a href='javascript:void(0)' class='dropbtn'>View</a><div class='dropdown-content'><a href='#' id='Tbar'>Tool bar</a><a href='#' id='Cbar'>Console bar</a><a href='#' id='2dbar'>2D</a><a href='#' id='3dbar'>3D</a></div></li>");
$panelcont.append("<li><a href='#' id='oPT'>Options</a></li>");
$panelcont.append("<li><a href='#'>Help</a></li>");
////////////////options
$( "#oPT" ).click(function() { $( "#options" ).toggle(); });
////////////////tool bar
$( "#Tbar" ).click(function() { $( "#game_info2" ).toggle(); });
////////////////console bar
$( "#Cbar" ).click(function() { $( "#game_info3" ).toggle(); });
////////////////calibrate
$( "#Calibar" ).click(function() { $( "#calibrate" ).toggle(); });
$("#calibrate").css({  'display':'none','opacity': '0.8', 'position': 'absolute','overflow': 'hidden','background-color': '#191919','height': '70%', 'width': '50%','right': '25%','top':'15%', 'border-radius': '10px 10px 10px 10px', 'border-style': 'double'  });
////////////////console bar
$( "#2dbar" ).click(function(){$("#game_info1").css("opacity", "1"); $("#Xaxis").css("opacity", "0");});
$( "#3dbar" ).click(function(){$("#game_info1").css("opacity", "0"); $("#Xaxis").css("opacity", "1");});
};

